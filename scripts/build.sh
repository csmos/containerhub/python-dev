#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-only
#
# Build container.
#
# Copyright (C) 2022 Daniel Gomez
# Author: Daniel Gomez <dagmcr@gmail.com>
set -e

if [[ $# -eq 0 ]] ; then
    target=local
else
    target=$1
fi

docker build -t $(basename $PWD):$target -f dockerfiles/python/slim/Dockerfile .
