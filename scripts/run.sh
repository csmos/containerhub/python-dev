#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-only
#
# Run container in interactive mode.
#
# Copyright (C) 2022 Daniel Gomez
# Author: Daniel Gomez <dagmcr@gmail.com>
set -e

if [[ $# -eq 0 ]] ; then
    target=local
else
    target=$1
fi

docker run --rm -it \
-u `id -u`:`id -g` \
-v /etc/passwd:/etc/passwd \
-v /etc/shadow:/etc/shadow \
-v $PWD:$PWD -w=$PWD \
$(basename $PWD):$target
