# SPDX-License-Identifier: GPL-2.0-only
#
# Dockerfile.
#
# Copyright (C) 2022 Daniel Gomez
# Author: Daniel Gomez <dagmcr@gmail.com>
FROM python:slim

RUN apt-get update && apt-get install --no-install-recommends -y \
    build-essential \
    ca-certificates \
    curl \
    git \
    libgnutls30 \
    pkg-config \
    ssh \
    ssl-cert \
    wget

# Pyenv
ENV PYENV_ROOT="/usr/local/opt/pyenv"
ENV PATH="$PYENV_ROOT/bin:$PATH"

# Poetry installation
ENV POETRY_HOME="/opt/poetry" \
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"
ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

RUN curl -sSL https://install.python-poetry.org | python3 -

# Python packages
RUN pip install \
    cookiecutter \
    nox \
    nox-poetry
